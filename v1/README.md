# Before using these scripts

Add these lines to your .bash_profile:
```bash
### EA Platform initialization ###
export EA_PLATFORM_DIR=/your/GOPATH/src/bitbucket.org/stefarf/ea-platform/v1
source $EA_PLATFORM_DIR/init.sh
```


# Usage

1.  Re-open terminal.
2.  Type `cd-eap`.
3.  Run:
    ```bash
    # to build all related apps and put it into docker staging directory
    ./build-app.sh

    # to build the docker image
    ./build-docker-image.sh

    # to push the docker image into docker repository
    ./push-docker-image.sh

    # to run all those three scripts
    ./complete-build-and-push.sh
    ```


# Build new version of Task Management app

To ensure that web always loads the updated js file, then the task js file is
versioned using this convention:

    task.v1.<minor>.<patch>.js

Replace the `<minor>` and `<patch>` according to latest git tag version.

To update the version, edit below files under the `v1` directory:

- `tm-components/config.json`
- `tm-components/webpack.config.js`
- `tools/src/tm-components-json.js`
- `build-app.sh`

Steps:

1. Build the Task Management app:

    ```
    cd-eap && \
        ./build-app.sh && \
        ./build-docker-image.sh
    ```

2. Test the app. If ok then:

    1. Update the version on those 4 files in the `v1` directory (see above).
    2. Rebuild the app and push docker image:
    
        ```
        cd-eap && ./complete-build-and-push.sh
        ```

    3. Git commit, git tag and git push tags.