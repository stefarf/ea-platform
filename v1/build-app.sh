#!/usr/bin/env bash
if [ -z "$EA_PLATFROM_INITIALIZED" ]; then echo EA Platform need to be initialized first; exit; fi


###
### WEB ###
###


## Compile Service Worker
#echo
#echo '### COMPILE Service Worker'
#cd $EA_PLATFORM_DIR/sw
#webpack
#mkdir -p $EA_PLATFORM_STAGING/web/_
#cp $EA_PLATFORM_DIR/base-ui/public/sw.js $EA_PLATFORM_STAGING/web/_/


## Compile Task Management components
VERSION=v1.5.2
##
#echo
#echo '### COMPILE Task Management Components'
#cd $EA_PLATFORM_DIR/tm-components
#webpack
#node $EA_PLATFORM_DIR/tools/src/tm-components-json.js
#cp config.json $EA_PLATFORM_DIR/base-ui/public/load/
#mkdir -p $EA_PLATFORM_STAGING/web/load
#cd $EA_PLATFORM_DIR/base-ui/public/load/
#cp config.json task.$VERSION.js $EA_PLATFORM_STAGING/web/load/
##


## Compile base-ui
#echo
#echo '### COMPILE Base UI'
#cd $EA_PLATFORM_DIR/base-ui
#npm run build
#rm -rf $EA_PLATFORM_STAGING/web/_/css $EA_PLATFORM_STAGING/web/_/js
#mkdir -p $EA_PLATFORM_STAGING/web/_
#cd dist; rm -f report.html; cp -a css js favicon.ico index.html manifest.json $EA_PLATFORM_STAGING/web/_/


###
### Go Binaries ###
###


## Compile Web Gateway
#echo
#echo '### COMPILE Web Gateway'
#cd $EA_PLATFORM_DIR/web-gw
#mkdir -p $EA_PLATFORM_STAGING/_web-gw
#env GOOS=linux GOARCH=amd64 go build -o $EA_PLATFORM_STAGING/_web-gw/web-gw
#cp web-gw.yaml $EA_PLATFORM_STAGING/_web-gw/


## Compile Services Runner
#echo
#echo '### COMPILE Services Runner'
#cd $EA_PLATFORM_DIR/run-svc
#mkdir -p $EA_PLATFORM_STAGING/_run-svc
#env GOOS=linux GOARCH=amd64 go build -o $EA_PLATFORM_STAGING/_run-svc/run-svc
#cp run.yaml $EA_PLATFORM_STAGING/_run-svc/


###
### JS ###
###


## Deploy JS Libraries
#echo
#echo '### DEPLOY JS Libraries'
#mkdir -p $EA_PLATFORM_STAGING
#rm -rf $EA_PLATFORM_STAGING/lib
#cp -a $EA_PLATFORM_DIR/lib $EA_PLATFORM_STAGING/


## Deploy Bundler
#echo
#echo '### DEPLOY Bundler'
#
## Copy all
#mkdir -p $EA_PLATFORM_STAGING/
#rm -rf $EA_PLATFORM_STAGING/bundler
#cp -a $EA_PLATFORM_DIR/bundler $EA_PLATFORM_STAGING/
#
## Copy only src
#mkdir -p $EA_PLATFORM_STAGING/bundler
#rm -rf $EA_PLATFORM_STAGING/bundler/src
#cp -a $EA_PLATFORM_DIR/bundler/src $EA_PLATFORM_STAGING/bundler/
## -- ##


## Deploy Task Management Server
#echo
#echo '### DEPLOY Task Management Server'
#mkdir -p $EA_PLATFORM_STAGING
#rm -rf $EA_PLATFORM_STAGING/tm-svr
#cp -a $EA_PLATFORM_DIR/tm-svr $EA_PLATFORM_STAGING/
