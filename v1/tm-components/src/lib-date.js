const TBL_MM2MON = {
    '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr',
    '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug',
    '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec'
};

const TBL_MON2MM = {
    Jan: '01', Feb: '02', Mar: '03', Apr: '04',
    May: '05', Jun: '06', Jul: '07', Aug: '08',
    Sep: '09', Oct: '10', Nov: '11', Dec: '12'
};

function dateNow() {
    let now = Date().toString();
    let m = now.match(/\w+ (\w+) (\d+) (\d+)/);
    let mm = TBL_MON2MM[m[1]];
    let dd = m[2];
    let yy = m[3];
    return `${yy}-${mm}-${dd}`;
}

export const DATE_NOW = dateNow();

// '2019-12-31' => '31-Dec-2019'
export function sqlDateToText(s) {
    if (!s) return '';
    let [y, m, d] = s.split('-');
    m = TBL_MM2MON[m];
    return `${d}-${m}-${y}`;
}

// '2019-12-31 01:02:03' => '31-Dec-2019 01:02:03'
export function sqlDateTimeToText(s) {
    if (!s) return '';
    const dt = s.split(' ');
    return `${sqlDateToText(dt[0])} ${dt[1]}`;
}