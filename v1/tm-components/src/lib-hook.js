import {apiFetchBundle} from './api';

const compiled = {};
const inProgress = {};

function fetchAndCompile(hash) {
    return new Promise(resolve => {
        if (compiled[hash]) {
            resolve(compiled[hash]);
            return;
        }

        if (inProgress[hash]) {
            inProgress[hash].push(resolve);
            return;
        }

        inProgress[hash] = [resolve];
        apiFetchBundle(hash)
            .then(js => {
                const bundle = (function () {
                    eval(js);
                    return this.bundle;
                }.call({}));

                compiled[hash] = bundle;
                inProgress[hash].forEach(resolve => {resolve(bundle);});
                delete inProgress[hash];
            });
    });
}

export function runHook(obj, name, noHook, ctx, ...args) {
    if (obj === undefined) {
        noHook();
        return;
    }

    if (!obj.hooks) {
        obj.hooks = {};
        if (obj.$hooks) {
            Object.keys(obj.$hooks).forEach(hookName => {
                const def = obj.$hooks[hookName];
                let f = () => {};
                if (def.simple)
                    f = Function(`return ${def.simple}`)();
                if (def.bundler)
                    f = Function('fc', `return ${def.bundler};`)(fetchAndCompile);
                obj.hooks[hookName] = f;
            });
            delete obj.$hooks;
        }
    }

    const f = obj.hooks[name];
    if (!f) {
        noHook();
        return;
    }
    f.call(ctx, ...args);
}