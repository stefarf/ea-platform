// export function nullOrUndefined(val) {
//     return (val === null || val === undefined);
// }

export function saveAsFile(data, type, filename) {
    const a = document.createElement('a');
    const url = window.URL.createObjectURL(new Blob([data], {type}));

    a.href = url;
    a.download = filename;
    document.body.appendChild(a);

    a.click();
    setTimeout(() => {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    }, 0);
}