import FormDate from './components/forms/FormDate.vue';
import FormEnum from './components/forms/FormEnum.vue';
import FormNumber from './components/forms/FormNumber.vue';
import FormPre from './components/forms/FormPre.vue';
import FormRadio from './components/forms/FormRadio.vue';
import FormText from './components/forms/FormText.vue';
import FormTextarea from './components/forms/FormTextarea.vue';
import ItemsSearchTable from './components/forms/ItemsSearchTable.vue';
import ItemsTable from './components/forms/ItemsTable.vue';

import WidgetAllTasks from './components/widgets/WidgetAllTasks.vue';
import WidgetButton from './components/widgets/WidgetButton.vue';
import WidgetError from './components/widgets/WidgetError.vue';
import WidgetList from './components/widgets/WidgetList.vue';
import WidgetParagraph from './components/widgets/WidgetParagraph.vue';
import WidgetSaveToCSV from './components/widgets/WidgetSaveToCSV.vue';
import WidgetTable from './components/widgets/WidgetTable.vue';
import WidgetTableAPIQuery from './components/widgets/WidgetTableAPIQuery.vue';
import WidgetTitle from './components/widgets/WidgetTitle.vue';

import PageIndex from './components/pages/PageIndex.vue';
import PageFromEmail from './components/pages/PageFromEmail.vue';
import PageLogin from './components/pages/PageLogin.vue';
import PageCreatePassword from './components/pages/PageCreatePassword.vue';
import PageWaitEmail from './components/pages/PageWaitEmail.vue';
import PageError from './components/pages/PageError.vue';
// import PageErrorLogin from './components/pages/PageErrorLogin.vue';
import PageTasks from './components/pages/PageTasks.vue';
import PageMenus from './components/pages/PageMenus.vue';
import PageNotFound from './components/pages/PageNotFound.vue';

export const components = {
    'w-all-tasks': WidgetAllTasks,
    'w-button': WidgetButton,
    'w-error': WidgetError,
    'w-list': WidgetList,
    'w-paragraph': WidgetParagraph,
    'w-save-to-csv': WidgetSaveToCSV,
    'w-table': WidgetTable,
    'w-table-api-query': WidgetTableAPIQuery,
    'w-title': WidgetTitle,

    'f-date': FormDate,
    'f-enum': FormEnum,
    'f-number': FormNumber,
    'f-pre': FormPre,
    'f-radio': FormRadio,
    'f-text': FormText,
    'f-textarea': FormTextarea,

    'fi-search-table': ItemsSearchTable,
    'fi-table': ItemsTable
};

export const routes = [
    {path: '/', component: PageIndex},
    {path: '/from-email', component: PageFromEmail},
    {path: '/login', component: PageLogin},
    {path: '/create-password', component: PageCreatePassword},
    {path: '/wait', component: PageWaitEmail},
    {path: '/error', component: PageError},
    // {path: '/error-login', component: PageErrorLogin},

    {path: '/t/:taskId', component: PageTasks, props: true},
    {path: '/m/*', component: PageMenus},

    {path: '*', component: PageNotFound}
];