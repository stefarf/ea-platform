import AppWithMenu from './components/base/AppWithMenu.vue';
import RenderView from './components/RenderView.vue';

export const viewerMixin = {
    components: {AppWithMenu, RenderView},
    data() {
        return {
            view: {title: ''},
            ctx: {pages: [{}]},
            render: 1
        };
    }
};

export const pageMixin = {
    components: {AppWithMenu}
};

export const widgetMixin = {
    props: ['ctx']
};

export const elementMixin = {
    props: ['ctx'],
    created() {
        if (this.ctx.data[this.ctx.element.name] === undefined)
            this.$set(
                this.ctx.data, this.ctx.element.name,
                this.ctx.element.nullValue !== undefined ? this.ctx.element.nullValue : null);
    }
};

export const itemsMixin = {
    props: ['ctx'],
    created() {
        if (this.ctx.sub) alert('Can not use fi (items) component for sub form');
        if (this.ctx.data[this.ctx.element.name] === undefined)
            this.$set(this.ctx.data, this.ctx.element.name, []);
    }
};