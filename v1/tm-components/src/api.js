const baseAPI = '/api/taskmgmt/v1';

function wrapFetch(self, url, init, ok, urlUnauthorized = '/login', urlErr = '/error') {
    let isok = false;
    fetch(url, init)
        .then(res => {
            if (res.ok) {
                isok = true;
                if (init.headers.Accept === 'application/json') return res.json();
                return res.text();
            }
            if (res.status === 401) {
                // Unauthorized
                self.$router.replace(urlUnauthorized);
                return res.text(); // to show the alert
            }
            self.$router.replace(urlErr);
            return res.text();
        })
        .catch(err => {
            console.error(err);
            self.$router.replace(urlErr);
        })
        .then(result => {
            if (isok) {
                ok(result);
                return;
            }
            if (result) alert(result);
        });
}

//
// Authentication
//

export function apiLogin(self, email, password, ok) {
    wrapFetch(self, baseAPI + '/login',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({email, password})
        },
        ok
    );
}

export function apiRegister(self, email, ok) {
    wrapFetch(
        self,
        baseAPI + '/register',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email})
        },
        ok
    );
}

export function apiCreatePassword(self, password, jwt, ok) {
    wrapFetch(
        self,
        baseAPI + '/create-password',
        {
            method: 'POST',
            headers: {
                'Auth': jwt,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({password})
        },
        ok
    );
}

//
// Menu List
//

export function apiListOfMenus(self, jwt, ok) {
    wrapFetch(
        self,
        baseAPI + '/menu-list',
        {
            method: 'POST',
            headers: {
                'Auth': jwt,
                'Accept': 'application/json'
            }
        },
        ok
    );
}

//
// All tasks
//

export function apiAllTasks(self, jwt, ok) {
    wrapFetch(
        self,
        baseAPI + '/tasks',
        {
            method: 'GET',
            headers: {
                'Auth': jwt,
                'Accept': 'application/json'
            }
        },
        ok
    );
}

//
// Index, menu and task pages
//

export function apiIndexPage(self, jwt) {
    wrapFetch(
        self,
        baseAPI + '/index',
        {
            method: 'GET',
            headers: {
                'Auth': jwt,
                'Accept': 'application/json'
            }
        },
        json => {
            self.ctx = json;
            self.render++;
        }
    );
}

export function apiMenuPage(self, jwt, menuPath) {
    wrapFetch(
        self,
        baseAPI + '/menus' + menuPath,
        {
            method: 'GET',
            headers: {
                'Auth': jwt,
                'Accept': 'application/json'
            }
        },
        json => {
            self.ctx = json;
            self.render++;
        }
    );
}

export function apiTaskPage(self, jwt, taskId) {
    wrapFetch(
        self,
        baseAPI + '/tasks/' + taskId,
        {
            method: 'GET',
            headers: {
                'Auth': jwt,
                'Accept': 'application/json'
            }
        },
        json => {
            self.ctx = json;
            self.render++;
        }
    );
}

//
// Submit form
//

export function apiSubmit(self, submitToken, data, ok) {
    wrapFetch(
        self,
        baseAPI + '/submit',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Auth': submitToken
            },
            body: JSON.stringify(data)
        },
        ok
    );
}

//
// Call node
//

export function apiCallWidget(self, apiToken, entry, data, ok) {
    wrapFetch(
        self,
        baseAPI + '/api',
        {
            method: 'POST',
            headers: {
                'Auth': apiToken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({entry, data})
        },
        ok
    );
}

//
// Bundle
//

export function apiFetchBundle(hash) {
    return fetch(baseAPI + '/bundle/' + hash, {method: 'GET'})
        .then(res => {
            if (res.ok) return res.text();
            console.error(res);
            throw res;
        });
}