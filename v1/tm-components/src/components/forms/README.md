# Form components types

## Handle simple data (prefixed with f-)

Types:
- date
- enum
- number
- pre
- radio
- text
- textarea

## Handle arrays of data / items (prefixed with fi-)

Types:
- search table
- table

# Form components specification

## For f and fi components:

- May utilize the ctx.element.beforeSubmit hook as a way to clean up data before submit.
  For example: `To parse the data as a number and if error then set to null.`

## For f components:
- Uses either elementMixin (for simple data) or itemsMixin.
- If the component changes / edits the ctx.data,
  then it must emit event `changed` and send the `{ctx}`.

## For fi components
- May open sub form if clicked.
- Can not be used as element within sub form.
- If the component changes / edits the ctx.data,
  then it must emit event `changed` and send the `{ctx, sub}`.
