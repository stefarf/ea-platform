package main

import (
	"fmt"
	"os"
	"os/exec"
	"time"
)

func main() {
	for _, ri := range config.Run {
		go runForever(ri, "")
	}
	for _, ri := range customRun {
		go runForever(ri, config.Custom.BaseCwd)
	}
	<-(chan int)(nil) // block forever, waiting for signal to terminate
}

func runForever(ri runInfo, baseCwd string) {
	msg := "### [EXECUTE] ###"
	for {
		cmd := exec.Command(ri.Cmd, ri.Args...)
		cmd.Dir = baseCwd + ri.Cwd
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if ri.Pause > 0 {
			time.Sleep(time.Second * time.Duration(ri.Pause))
		}
		fmt.Println(msg, ri)
		cmd.Run()

		time.Sleep(time.Second * time.Duration(config.ReRun.Pause))
		msg = "### [RE-RUN] ###"
	}
}
