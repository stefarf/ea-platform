package main

import (
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type runInfo struct {
	Cwd   string
	Cmd   string
	Args  []string
	Pause int
}

var config struct {
	ReRun struct {
		Pause int
	} `yaml:"re_run"`
	Run    []runInfo
	Custom struct {
		RunConfig string `yaml:"run_config"`
		BaseCwd   string `yaml:"base_cwd"`
	}
}

var customRun []runInfo

func init() {
	readConfig("### Read run.yaml ###", "/apps/run.yaml", &config)
	if config.Custom.RunConfig != "" {
		readConfig("", config.Custom.RunConfig, &customRun)
	}
}

func readConfig(msg, full string, v interface{}) {
	fmt.Println(msg)
	errMsg := fmt.Sprintf("error reading/parsing configuration file '%s'", full)
	b, err := ioutil.ReadFile(full)
	iferr.Exit(err, errMsg)
	iferr.Exit(yaml.Unmarshal(b, v), errMsg)
}
