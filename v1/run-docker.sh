#!/usr/bin/env bash
if [ -z "$EA_PLATFROM_INITIALIZED" ]; then echo EA Platform need to be initialized first; exit; fi

RUN_NAME=ea-platform
RUN_PORT=8080

docker stop $RUN_NAME 2>/dev/null
docker run \
    --name $RUN_NAME \
    -it --rm \
    -p $RUN_PORT:2000 \
    -e EA_CONFIG_MODE=dev \
    $EA_PLATFORM_IMAGE bash
docker system prune -f
