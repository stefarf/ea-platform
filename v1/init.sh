#!/usr/bin/env bash
export EA_PLATFROM_INITIALIZED=1
export EA_PLATFORM_STAGING=$EA_PLATFORM_DIR/DOCKER/staging
export EA_PLATFORM_IMAGE=stefarf/ea-platform:1

alias cd-eap="cd $EA_PLATFORM_DIR"
echo Base EA Platform initialized
