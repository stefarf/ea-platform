const dblib = require('db-lib');
const config = require('./config').load();

async function newDB() {
    const db = await dblib.connect(config.mysql);
    let isQuery = false;
    let isEnd = false;
    return {
        async query(sql, args) {
            if (isEnd) throw 'Error db connection has ended';
            isQuery = true;
            return await db.query(sql, args);
        },

        async end() {
            if (isEnd) return;
            if (isQuery) await db.end();
            isEnd = true;
        }
    };
}

async function newTX() {
    const db = await dblib.connect(config.mysql);
    let isBegin = false;
    let isEnd = false;
    return {
        async query(sql, args) {
            if (isEnd) throw 'Error db connection has ended';
            if (!isBegin) {
                await db.begin();
                isBegin = true;
            }
            console.log(sql);
            console.log(args);
            return await db.query(sql, args);
        },

        async commit() {
            if (isEnd) throw 'Error db connection has ended';
            if (!isBegin) return;
            await db.commit();
            await db.end();
            isEnd = true;
        },

        async rollback() {
            if (isEnd) throw 'Error db connection has ended';
            if (!isBegin) return;
            await db.rollback();
            await db.end();
            isEnd = true;
        },

        async end() {
            if (isEnd) return;
            if (!isBegin) return;
            await db.commit();
            await db.end();
            isEnd = true;
        }
    };
}

module.exports = async function () {return {db: await newDB(), tx: await newTX()};};

