const httplib = require('http-lib');
const config = require('../config').load();
const {generateView} = require('../view');

module.exports = async (req, res, dbtx, auth) => {
    const menuPath = req.path.slice(10); // Skip the first /v1/menus/
    if (!menuPath) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const args = req.path.slice(10).split('/');
    const menu = args.shift();

    let sub;
    if (args.length) sub = args.shift(); else sub = 'index';

    // Get view config
    let menuCfg;
    try {
        menuCfg = config.menus[menu][sub];
    } catch (e) {
        console.error(e);
        res.status(httplib.statusBadRequest).send('Unknown menu');
        return;
    }

    if (!menuCfg) {
        res.status(httplib.statusBadRequest).send('Unknown menu');
        return;
    }

    const viewName = menuCfg.view;
    try {
        res.send(await generateView(viewName, args, dbtx, auth, {
            ctx: {closeAfterSubmit: menuCfg.closeAfterSubmit, urlAfterSubmit: menuCfg.urlAfterSubmit}
        }));
    } catch (e) {
        console.error(e);
        res.status(e.code).send(e.err);
    }
};