const httplib = require('http-lib');
const multiList = require('tm-lib').multiList;

const config = require('../config').load();
const query = require('../query');
const tasksValidate = require('../tasks-validate');

module.exports = async (req, res, dbtx, auth) => {
    try {
        const rows = [];
        (await tasksValidate.assigned(dbtx,
            await query.allAssignedTasksByEmail(dbtx.db, auth.email)))
            .forEach(task => {
                rows.push({
                    $table: task.table_name,
                    $section: task.section,
                    $title: task.title,
                    $subtitle: task.subtitle,
                    $url: '/t/' + task.id
                });
            });
        res.send(multiList.fromRows(rows));
    } catch (e) {
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};