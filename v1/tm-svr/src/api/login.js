const check = require('check');
const httplib = require('http-lib');
const sha256 = require('js-sha256').sha256;

const config = require('../config').load();
const jwt = require('../jwt');
const policy = require('../policy');

module.exports = async (req, res, dbtx) => {
    const body = req.body;
    if (!check.isObject(body)) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }
    const {email, password} = req.body;
    if (!check.isString(email) || !email ||
        !check.isString(password) || !password) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const err = policy.passwordPolicy(password);
    if (err) {
        res.status(httplib.statusUnauthorized).send(err);
        return;
    }

    try {
        const q = `SELECT
                       1 AS ok
                   FROM task.auth
                   WHERE
                       email = ? AND
                       hash = ?`;
        const args = [email, sha256(password)];
        const rst = await dbtx.db.query(q, args);
        if (rst.length !== 1 || rst[0].ok !== 1) {
            res.status(httplib.statusUnauthorized).send('Unauthorized access');
            return;
        }
        res.send({
            jwt: await jwt.signHS256(
                {email}, config.jwt.key,
                {subject: 'login', expiresIn: config.jwt.loginExpiresIn})
        });
    } catch (e) {
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};