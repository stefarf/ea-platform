const httplib = require('http-lib');
const config = require('../config').load();
const query = require('../query');
const {generateView} = require('../view');

module.exports = async (req, res, dbtx, auth) => {
    const {taskId} = req.params;

    let taskInfo;
    try {
        taskInfo = await query.getTaskInfo(dbtx.db, taskId, auth.email);
    } catch (e) {
        console.error(e);
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const task = config.tasks[taskInfo.taskName];
    if (!task || !task.view) {
        res.status(httplib.statusInternalServerError).send('Internal error');
        return;
    }

    const viewName = task.view;

    const data = {};
    while (taskInfo.keys.length) data[taskInfo.keys.shift()] = taskInfo.vals.shift();
    const args = [];
    (task.params || []).forEach(key => {args.push(data[key]);});

    try {
        res.send(await generateView(viewName, args, dbtx, auth, {
            taskId,
            ctx: {closeAfterSubmit: false, urlAfterSubmit: '/'}
        }));
    } catch (e) {
        console.error(e);
        res.status(e.code).send(e.err);
    }
};