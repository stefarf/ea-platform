const access = require('../access');
const config = require('../config').load();

module.exports = async (req, res, dbtx, auth) => {
    const menuList = [];
    for (const item of config.menuList) {
        const menus = [];
        for (const menu of item.menus)
            if (menu.allTraits || menu.anyTraits) {
                if (menu.allTraits && !await access.allTraits(dbtx.db, auth.email, menu.allTraits))
                    menus.push(menu);
                if (menu.anyTraits && !await access.anyTraits(dbtx.db, auth.email, menu.anyTraits))
                    menus.push(menu);
            } else
                menus.push(menu);
        if (menus.length) menuList.push({section: item.section, menus});
    }
    res.send(menuList);
};