const check = require('check');
const httplib = require('http-lib');
const URL = require('url').URL;

const config = require('../config').load();
const jwt = require('../jwt');
const mailer = require('../mailer');
const template = require('../template');

module.exports = async (req, res) => {
    const body = req.body;
    if (!check.isObject(body)) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }
    const {email} = body;
    if (!check.isString(email) || !email) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    try {
        const u = new URL(config.baseURL);
        u.pathname = '/create-password';
        u.searchParams.set('jwt', await jwt.signHS256(
            {email}, config.jwt.key,
            {subject: 'password', expiresIn: config.jwt.registerExpiresIn}));

        const url = u.toString();
        console.log('url:', url);

        if (config.sendEmail.send) {
            const subject = '[Enterprise Architecture] Next step: Create your password to open the app';
            const to = config.sendEmail.to ? config.sendEmail.to : [email];
            if (to.length) {
                console.log(`Send registration token for ${email} to ${to}`);
                const html = template.register({
                    title: 'Your email is registered, next: create your password',
                    emailFor: email,
                    messages: [
                        'Hi,',
                        "You received this email because your email address was registered to EA App. " +
                        "If it wasn't you who registered, then just ignore this email."
                    ],
                    url,
                    validFor: config.jwt.registerValidFor
                });
                await mailer.send(to, subject, html);
            }
        }

        res.status(httplib.statusOK).end();
    } catch (e) {
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};