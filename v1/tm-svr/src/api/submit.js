const check = require('check');
const httplib = require('http-lib');

const config = require('../config').load();
const jwt = require('../jwt');
const lib = require('../lib');
const query = require('../query');
const {paramsArgsToData} = require('../view');

module.exports = async (req, res, dbtx, auth) => {
    const data = req.body;
    if (!check.isObject(data)) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const view = config.views[auth.view];

    // Client shall not modify the params-args.
    // To ensure then params-args are re-injected to data.
    try {paramsArgsToData(view.params || [], auth.args, data);} catch (e) {
        console.error(e);
        res.status(httplib.statusBadRequest).send(e);
        return;
    }

    const thisSubmit = {...dbtx, auth};
    try {
        const f = view.pages[auth.pageIndex].form.buttons[auth.buttonIndex].submit;
        const submitResult = await lib.asyncCall(f, thisSubmit, data);
        if (auth.taskId !== undefined) await query.submitTask(dbtx.tx, auth.taskId);
        await dbtx.tx.commit();
        res.status(httplib.statusOK).send(submitResult || {});
    } catch (e) {
        await dbtx.tx.rollback();
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};