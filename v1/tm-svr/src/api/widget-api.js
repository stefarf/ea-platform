const check = require('check');
const httplib = require('http-lib');

const config = require('../config').load();
const jwt = require('../jwt');
const lib = require('../lib');
const {paramsArgsToData} = require('../view');

module.exports = async (req, res, dbtx, auth) => {
    const {entry, data} = req.body;
    if (!entry || !check.isString(entry) || !check.isObject(data)) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const view = config.views[auth.view];

    // Client shall not modify the params-args.
    // To ensure then params-args are re-injected to data.
    try {paramsArgsToData(view.params || [], auth.args, data);} catch (e) {
        console.error(e);
        res.status(httplib.statusBadRequest).send(e);
        return;
    }

    const thisAPI = {...dbtx, auth};
    try {
        const f = config.views[auth.view].pages[auth.pageIndex].widgets[auth.widgetIndex].api[entry];
        res.send(await lib.asyncCall(f, thisAPI, data));
    } catch (e) {
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};