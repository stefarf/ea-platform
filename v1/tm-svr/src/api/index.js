const config = require('../config').load();
const {generateView} = require('../view');

module.exports = async (req, res, dbtx, auth) => {
    const viewName = config.index.view;
    try {
        res.send(await generateView(viewName, [], dbtx, auth, {
            ctx: {closeAfterSubmit: config.index.closeAfterSubmit, urlAfterSubmit: config.index.urlAfterSubmit}
        }));
    } catch (e) {
        console.error(e);
        res.status(e.code).send(e.err);
    }
};