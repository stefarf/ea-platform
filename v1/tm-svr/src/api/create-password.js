const check = require('check');
const httplib = require('http-lib');

const config = require('../config').load();
const sha256 = require('js-sha256').sha256;
const jwt = require('../jwt');
const policy = require('../policy');

module.exports = async (req, res, dbtx, auth) => {
    const body = req.body;
    if (!check.isObject(body)) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }
    const {password} = body;
    if (!check.isString(password) || !password) {
        res.status(httplib.statusBadRequest).send('Bad request');
        return;
    }

    const err = policy.passwordPolicy(password);
    if (err) {
        res.status(httplib.statusBadRequest).send(err);
        return;
    }

    const {email} = auth;
    try {
        await dbtx.db.query(
            `REPLACE INTO task.auth
                 SET
                     email = ?,
                     hash  = ?`,
            [email, sha256(password)]);
        res.send({
            jwt: await jwt.signHS256(
                {email}, config.jwt.key,
                {subject: 'login', expiresIn: config.jwt.loginExpiresIn})
        });
    } catch (e) {
        console.error(e);
        res.status(httplib.statusInternalServerError).send('Internal error');
    }
};