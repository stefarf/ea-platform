const cron = require('node-cron');
const URL = require('url').URL;

const config = require('./config').load();
const dbtxNew = require('./dbtx');
const jwt = require('./jwt');
const lib = require('./lib');
const mailer = require('./mailer');
const query = require('./query');
const template = require('./template');

if (config.notification && config.notification.schedules)
    config.notification.schedules.forEach(sched => {
        console.log('Schedule BG NOTIF:', sched);
        cron.schedule(sched, () => {
            console.log('BG NOTIF IS STARTED');
            job().then(() => {console.log('BG NOTIF DONE');});
        }, {timezone: 'Asia/Jakarta'});
    });
else {
    // Do nothing so this background job would not exit if there is no
    // notification schedules defined
    setInterval(() => {}, 60000);
}

function whereAndArgs(filter) {
    let filterWhere = '';
    const filterArgs = [];
    ['table', 'section', 'title', 'subtitle'].forEach(key => {
        const val = filter[key];
        if (!val) return;

        if (key === 'table') key = 'table_name';
        if (filterWhere) filterWhere += ' AND ';
        filterWhere += `${key} = ?`;
        filterArgs.push(val);
    });
    return {filterWhere, filterArgs};
}

async function job() {
    if (!config.sendEmail.send) return;

    const dbtx = await dbtxNew();

    try {
        await lib.asyncForEach(config.notification.by, async by => {
            const {filterWhere, filterArgs} = whereAndArgs(by.filter);
            const tasksByEmail = await query.getTasksByEmail(dbtx.db, filterWhere, filterArgs);

            await lib.asyncForEach(Object.keys(tasksByEmail), async email => {
                const numOfTasks = tasksByEmail[email];

                let more = 0;
                if (numOfTasks > config.notification.limit) more = numOfTasks - config.notification.limit;

                const u = new URL(config.baseURL);
                u.pathname = '/from-email';
                u.searchParams.set('jwt', await jwt.signHS256(
                    {email}, config.jwt.key,
                    {subject: 'login', expiresIn: config.jwt.loginExpiresIn}));

                const url = u.toString();
                console.log('url:', url);

                const tasks = await query.getTasksForNotification(dbtx.db, filterWhere, filterArgs, email, config.notification.limit);
                if (tasks.length) {
                    const html = template.notify({
                        title: by.subject,
                        emailFor: email,
                        messages: by.messages,
                        tasks,
                        more,
                        url,
                        validFor: config.jwt.loginValidFor
                    });
                    const to = config.sendEmail.to ? config.sendEmail.to : [email];
                    if (to.length) await mailer.send(to, `[Enterprise Architecture] ${by.subject}`, html);
                }
            });
        });
    } catch (e) {console.error(e);} finally {
        try {
            await dbtx.db.end();
            await dbtx.tx.end();
        } catch (e) {console.error(e);}
    }
}