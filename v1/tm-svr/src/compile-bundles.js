const check = require('check');

const config = require('./config').load();
const global = require('./global');
const lib = require('./lib');

async function processHook(obj, $bundle) {
    if (!obj.hooks) return;

    obj.$hooks = {};
    await lib.asyncForEach(Object.keys(obj.hooks), async hookName => {
        const def = obj.hooks[hookName];
        if (check.isFunction(def))
            obj.$hooks[hookName] = {simple: obj.hooks[hookName].toString()};
        else if (check.isObject(def) && check.isFunction(def.bundler))
            obj.$hooks[hookName] = {bundler: await def.bundler($bundle, hookName, global)};
    });
    delete obj.hooks;
}

module.exports = async function () {
    await lib.asyncForEach(Object.keys(config.views), async viewName => {
        const view = config.views[viewName];
        view.$bundle = {};

        // hooks at view level
        await processHook(view, view.$bundle);

        if (view.pages && view.pages.length)
            await lib.asyncForEach(view.pages, async page => {
                // hooks at page level
                await processHook(page, view.$bundle);

                if (page.widgets)
                    await lib.asyncForEach(page.widgets, async widget => {
                        // hooks at widget level
                        await processHook(widget, view.$bundle);
                    });

                if (page.form) {
                    // hooks at form level
                    await processHook(page.form, view.$bundle);

                    if (page.form.elements && page.form.elements.length)
                        await lib.asyncForEach(page.form.elements, async element => {
                            // hooks at element level
                            await processHook(element, view.$bundle);
                        });
                }
            });
    });
};