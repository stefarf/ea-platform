const path = require('path');

let config = {};
let isFixed = false;

function fix(config) {
    config.server = config.server || {port: 2001};

    config.baseURL = config.baseURL || 'http://localhost:8080';

    config.jwt = config.jwt || {
        key: 'secret',

        loginExpiresIn: '3h',
        loginValidFor: '3 hours',

        registerExpiresIn: '1h',
        registerValidFor: '1 hour',

        apiExpiresIn: '1h',
        buttonExpiresIn: '1h'
    };

    config.sendEmail = config.sendEmail || {send: false};

    config.mysql = config.mysql || {
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'mysql',
        cerFile: ''
    };

    config.menuList = config.menuList || [];
    config.index = config.index || {view: 'index'};
    config.menus = config.menus || {};
    config.tasks = config.tasks || {};
    config.views = config.views || {index: {title: 'INDEX'}};
    config.dictionaries = config.dictionaries || {};
    config.buckets = config.buckets || {default: {tasksLimit: 20}};

    return config;
}

module.exports = {
    test: {
        fix
    },
    load() {
        if (!isFixed) {
            let tmConfig = 'custom/tm-config';
            if (process.env.TM_CONFIG) tmConfig = process.env.TM_CONFIG;
            config = fix(require(`../../${tmConfig}`));
            isFixed = true;
            return config;
        }
        return config;
    }
};