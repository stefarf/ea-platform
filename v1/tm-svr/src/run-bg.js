const check = require('check');

const getAssignments = require('./tasks-assign');
const config = require('./config').load();
const dbtxNew = require('./dbtx');
const lib = require('./lib');
const query = require('./query');
const tasksValidate = require('./tasks-validate');

async function checkTasks(dbtx) {
    console.log('### CHECK TASKS ###');
    await tasksValidate.assigned(dbtx, await query.getOpenTasks(dbtx.db));
}

async function cleanUp(dbtx) {
    console.log('### CLEAN UP TASKS ###');
    await query.completeTasks(dbtx.db);
    await query.expireTasks(dbtx.db);
}

async function beforeCreateTasks(dbtx) {
    console.log('### BEFORE CREATE TASKS ###');
    await lib.asyncForEach(Object.keys(config.tasks), async taskName => {
        const task = config.tasks[taskName];
        if (task.beforeCreate) await lib.asyncCall(task.beforeCreate, dbtx);
    });
}

async function createTasks(dbtx) {
    console.log('### CREATE TASKS ###');
    await lib.asyncForEach(Object.keys(config.tasks), async taskName => {
        const task = config.tasks[taskName];
        const params = task.params || [];
        if (task.find) {
            const rows = await lib.asyncCall(task.find, dbtx);
            if (check.isArray(rows) && rows.length)
                await lib.asyncForEach(rows, async row => {
                    const email = row.$email;

                    const vals = [];
                    params.forEach(col => {vals.push(row[col]);});

                    const jsKeys = JSON.stringify(params);
                    const jsVals = JSON.stringify(vals);

                    const table = row.$table || 'Table';
                    const section = row.$section || 'Section';
                    const title = row.$title || 'Title';
                    const subtitle = row.$subtitle || 'Subtitle';

                    if (!await query.taskExists(dbtx.db, email, taskName, jsKeys, jsVals)) {
                        console.log('Create task:', email, taskName, jsKeys, jsVals);
                        await query.createTask(dbtx.db, email, taskName, jsKeys, jsVals,
                            table, section, title, subtitle);
                    }
                });
        }
    });
}

async function assignTasks(dbtx) {
    console.log('### ASSIGN TASKS ###');
    const assignments = await getAssignments(dbtx);
    await lib.asyncForEach(Object.keys(assignments), async email => {
        const toBeAssigned = assignments[email];
        await lib.asyncForEach(Object.keys(toBeAssigned), async taskName => {
            const task = config.tasks[taskName];
            const limit = toBeAssigned[taskName];
            const taskIDs = await query.taskIDsToBeAssigned(dbtx.db, email, taskName, limit);
            await lib.asyncForEach(taskIDs, async id => {
                await query.assignTask(dbtx.db, id, task.expires || 7);
            });
        });
    });
}

async function job() {
    const dbtx = await dbtxNew();
    try {
        await checkTasks(dbtx);
        await cleanUp(dbtx);
        await beforeCreateTasks(dbtx);
        await createTasks(dbtx);
        await assignTasks(dbtx);
    } catch (e) {console.error(e);} finally {
        try {
            await dbtx.db.end();
            await dbtx.tx.end();
        } catch (e) {console.error(e);}
    }
}

function runLoop(asyncJob, delayStart, delayNext) {
    const loop = () => {
        asyncJob().then(() => {
            console.log('### WAIT FOR NEXT BG JOB ###');
            setTimeout(loop, delayNext * 1000);
        });
    };
    setTimeout(loop, delayStart * 1000);
}

runLoop(job, 0, 60 * 30);