const fs = require('fs');

exports.asyncReadFile = function (filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', (err, content) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(content);
        });
    });
};

function clone(v) {
    return JSON.parse(JSON.stringify(v));
}

exports.clone = clone;

exports.asyncForEach = async function (arr, cb) {
    for (const i of arr) await cb(i);
};

exports.asyncCall = async function (f, self, ...args) {
    if (f.constructor.name === 'AsyncFunction')
        return await f.call(self, ...args);
    else
        return f.call(self, ...args);
};
