exports.getOpenTasks = async function (db) {
    const q = `SELECT *
               FROM task.task
               WHERE
                   state IN ('created', 'assigned')`;
    return await db.query(q);
};

exports.completeTasks = async function (db) {
    const q = `UPDATE task.task
               SET
                   state = 'done'
               WHERE
                   state = 'submitted' AND
                   now() > expires`;
    await db.query(q);
};

exports.expireTasks = async function (db) {
    const q = `UPDATE task.task
               SET
                   state = 'expired'
               WHERE
                   state = 'assigned' AND
                   now() > expires`;
    await db.query(q);
};

exports.taskExists = async function (db, email, taskName, jsKeys, jsVals) {
    const q = `SELECT
                   count(*) AS cnt
               FROM task.task
               WHERE
                   email = ? AND
                   task = ? AND
                   js_keys = ? AND
                   js_vals = ? AND
                   state NOT IN ('done', 'expired')`;
    const args = [email, taskName, jsKeys, jsVals];
    const rows = await db.query(q, args);
    return rows[0].cnt;
};

exports.createTask = async function (db, email, taskName, jsKeys, jsVals, tableName, section, title, subtitle) {
    const q = `INSERT INTO task.task
               SET
                   email      = ?,
                   task       = ?,
                   js_keys    = ?,
                   js_vals    = ?,
                   table_name = ?,
                   section    = ?,
                   title      = ?,
                   subtitle   = ?,
                   state      = 'created',
                   created    = now()`;
    const args = [email, taskName, jsKeys, jsVals, tableName, section, title, subtitle];
    await db.query(q, args);
};

exports.taskIDsToBeAssigned = async function (db, email, taskName, limit) {
    const q = `
        SELECT
            id
        FROM task.task
        WHERE
            email = ? AND
            task = ? AND
            state = 'created'
        ORDER BY created, id
        LIMIT ?`;
    const args = [email, taskName, limit];
    const rows = await db.query(q, args);
    const taskIds = [];
    rows.forEach(row => {taskIds.push(row.id);});
    return taskIds;
};

exports.assignTask = async function (db, id, daysExpire) {
    const q = `
        UPDATE task.task
        SET
            state    = 'assigned',
            assigned = now(),
            expires  = date_add(now(), INTERVAL ? DAY)
        WHERE
            id = ?`;
    const args = [daysExpire, id];
    await db.query(q, args);
};

exports.allAssignedTasksByEmail = async function (db, email) {
    const q = `
        SELECT *
        FROM task.task
        WHERE
            state = 'assigned' AND
            email = ?
        ORDER BY table_name, section, title, subtitle`;
    const args = [email];
    return await db.query(q, args);
};

exports.getTaskInfo = async function (db, id, email) {
    const q = `
        SELECT
            task,
            js_keys,
            js_vals
        FROM task.task
        WHERE
            id = ? AND
            email = ? AND
            state = 'assigned'`;
    const args = [id, email];
    const rows = await db.query(q, args);
    if (rows.length !== 1) throw 'Error task not found';
    const row = rows[0];
    return {
        taskName: row.task,
        keys: JSON.parse(row.js_keys),
        vals: JSON.parse(row.js_vals)
    };
};

exports.getTasksByEmail = async function (db, where, args) {
    const tasks = {};
    const q = `
        SELECT email, count(*) AS cnt
        FROM task.task
        WHERE state = 'assigned' AND ${where}
        GROUP BY email`;
    const rows = await db.query(q, args);
    rows.forEach(row => {tasks[row.email] = row.cnt;});
    return tasks;
};

exports.getTasksForNotification = async function (db, filterWhere, filterArgs, email, limit) {
    const q = `
        SELECT title, subtitle FROM task.task
        WHERE state = 'assigned' AND email = ? AND ${filterWhere}
        ORDER BY table_name, section, title, subtitle
        LIMIT ${limit}`;
    const args = [email, ...filterArgs];
    const tasks = await db.query(q, args);
    let no = 1;
    tasks.forEach(task => {
        task.no = no;
        no++;
    });
    return tasks;
};

exports.submitTask = async function (tx, id) {
    const q = `
        UPDATE task.task
        SET
            state     = 'submitted',
            submitted = now()
        WHERE
            id = ?`;
    const args = [id];
    console.log(q, args);
    await tx.query(q, args);
};

exports.deleteTask = async function (db, id) {
    const q = `
        DELETE
        FROM task.task
        WHERE
            id = ?`;
    const args = [id];
    await db.query(q, args);
};