const config = require('./config').load();
const lib = require('./lib');
const query = require('./query');

exports.assigned = async function (dbtx, rows) {
    const assignedTasks = [];
    for (const row of rows) {
        const task = config.tasks[row.task];
        if (!task) {
            await query.deleteTask(dbtx.db, row.id);
            continue;
        }

        const keys = JSON.parse(row.js_keys);
        const vals = JSON.parse(row.js_vals);

        const data = {};
        while (keys.length) data[keys.shift()] = vals.shift();

        if (task.validate) {
            const valid = await lib.asyncCall(task.validate, {...dbtx, ...row}, data);
            if (valid) assignedTasks.push(row);
            else await query.submitTask(dbtx.db, row.id);
        } else
            assignedTasks.push(row);
    }
    return assignedTasks;
};