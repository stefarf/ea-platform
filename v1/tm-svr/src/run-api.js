// const cors = require('cors');
const express = require('express');

const compileBundles = require('./compile-bundles');
const config = require('./config').load();
const dbtxNew = require('./dbtx');
const global = require('./global');
const jwt = require('./jwt');
const lib = require('./lib');

const app = express();
app.use(express.json()); // app.use(cors());

function api(module) {
    const handler = async (req, res) => {
        try {
            await lib.asyncCall(require(module), {}, req, res);
        } catch (e) {console.error(e);}
    };
    return (req, res) => {handler(req, res).then(() => {});};
}

function apiDB(module) {
    const handler = async (req, res) => {
        const dbtx = await dbtxNew();
        try {
            await lib.asyncCall(require(module), {}, req, res, dbtx);
        } catch (e) {console.error(e);} finally {
            try {
                await dbtx.db.end();
                await dbtx.tx.end();
            } catch (e) {console.error(e);}
        }
    };
    return (req, res) => {handler(req, res).then(() => {});};
}

function apiAuthDB(module, subject) {
    const handler = async (req, res) => {
        let auth;
        try {
            auth = await jwt.verifyHS256(req.get('Auth'), config.jwt.key, {subject});
        } catch (e) {
            res.status(401).end(); // not authorized
            return;
        }

        const dbtx = await dbtxNew();
        try {
            await lib.asyncCall(require(module), {}, req, res, dbtx, auth);
        } catch (e) {console.error(e);} finally {
            try {
                await dbtx.db.end();
                await dbtx.tx.end();
            } catch (e) {console.error(e);}
        }
    };
    return (req, res) => {handler(req, res).then(() => {});};

}

function runAPI() {
    app.get('/v1/bundle/:hash', (req, res) => {
        const file = global.hashToFile[req.params.hash];
        lib.asyncReadFile(file)
            .then(js => {
                res.send(js);
            })
            .catch(err => {
                res.status(400).end(); // bad request
            });
    });

    app.post('/v1/register', api('./api/register'));
    app.post('/v1/login', apiDB('./api/login'));
    app.post('/v1/create-password', apiAuthDB('./api/create-password', 'password'));

    app.post('/v1/menu-list', apiAuthDB('./api/menu-list', 'login'));
    app.get('/v1/index', apiAuthDB('./api/index', 'login'));
    app.get('/v1/menus/*', apiAuthDB('./api/menu', 'login'));

    app.get('/v1/tasks', apiAuthDB('./api/all-tasks', 'login'));
    app.get('/v1/tasks/:taskId', apiAuthDB('./api/task', 'login'));

    app.post('/v1/submit', apiAuthDB('./api/submit', 'submit'));
    app.post('/v1/api', apiAuthDB('./api/widget-api', 'api'));

    app.listen(config.server.port, () => {
        console.log('TaskMgmt API server is running on port:', config.server.port);
    });
}

async function runInit() {
    const dbtx = await dbtxNew();
    try {
        await lib.asyncForEach(Object.keys(config.views), async viewName => {
            const view = config.views[viewName];
            if (view.init) await lib.asyncCall(view.init, dbtx);
        });
    } catch (e) {console.error(e);} finally {
        try {
            await dbtx.db.end();
            await dbtx.tx.end();
        } catch (e) {console.error(e);}
    }
}

async function main() {
    await compileBundles();
    await runInit();
    runAPI();
}

main().then(() => {});