const config = require('./config').load();
const lib = require('./lib');

const ONE_MINUTE = 60000;
const cache = {};

async function getTraits(db, email) {
    email = email.trim().toLowerCase();

    const c = cache[email];
    if (c && c.expires >= Date.now()) return c.traits;

    const traits = await lib.asyncCall(config.getTraits, {db}, email);
    cache[email] = {
        traits,
        expires: Date.now() + ONE_MINUTE
    };
    return traits;
}

async function anyTraits(db, email, any) {
    if (!config.getTraits) return false;
    const traits = await getTraits(db, email);
    for (const t of any) if (traits.includes(t)) return false;
    return true;
}

async function allTraits(db, email, all) {
    if (!config.getTraits) return false;
    const traits = await getTraits(db, email);
    for (const t of all) if (!traits.includes(t)) return true;
    return false;
}

module.exports = {
    anyTraits,
    allTraits
};