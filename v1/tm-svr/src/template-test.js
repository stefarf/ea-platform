const template = require('./template');

function notify() {
    return template.notify({
        title: 'This is testing',
        emailFor: 'user@domain',
        messages: ['Hi,', 'This is for testing the template.'],
        tasks: [
            {no: 1, title: 'Task 1', subtitle: 'Do it now!'},
            {no: 2, title: 'Task 2', subtitle: 'Do it now!'}
        ],
        more: 3,
        url: '',
        validFor:'12 hours'
    });
}

function register() {
    return template.register({
        title: 'Your email is registered, next: create your password',
        emailFor: 'user@domain',
        messages: [
            'Hi,',
            "You received this email because your email address was registered to EA App. " +
            "If it wasn't you who registered, then just ignore this email."
        ],
        url: '',
        validFor: '1 hour'
    });
}

console.log(notify());