const config = require('./config').load();

module.exports = getAssignments;

// Returns buckets = {
//     default: {
//         email: {
//             task: {
//                 created,
//                 assigned
//             }
//         }
//     }
// };
async function fillInBuckets(db) {
    const buckets = {};
    const rows = await db.query(
            `SELECT
                 email,
                 task,
                 state,
                 count(*) AS cnt
             FROM task.task
             WHERE
                 state IN ('created', 'assigned')
             GROUP BY email, task, state`);
    rows.forEach(row => {
        const task = config.tasks[row.task];
        // const task = config.tasks[row.task] || {}; // TODO for debug
        if (!task) return;

        task.bucket = task.bucket || 'default';
        if (!buckets[task.bucket]) buckets[task.bucket] = {};
        const bucket = buckets[task.bucket];

        const email = row.email.toLowerCase();
        if (!bucket[email]) bucket[email] = {};
        const perEmail = bucket[email];

        if (!perEmail[row.task]) perEmail[row.task] = {};
        const perTask = perEmail[row.task];

        perTask[row.state] = row.cnt;
    });

    // Set .created and .assigned to number
    Object.keys(buckets).forEach(bucketName => {
        const bucket = buckets[bucketName];
        Object.keys(bucket).forEach(email => {
            const perEmail = bucket[email];
            Object.keys(perEmail).forEach(taskName => {
                const perTask = perEmail[taskName];
                if (perTask.created === undefined) perTask.created = 0;
                if (perTask.assigned === undefined) perTask.assigned = 0;
            });
        });
    });

    return buckets;
}

function getTasksLimit(bucketName) {
    config.buckets[bucketName] = config.buckets[bucketName] || {};
    const bucket = config.buckets[bucketName];
    if (bucket.tasksLimit === undefined) bucket.tasksLimit = 20;
    return bucket.tasksLimit;
}

function findTaskWithLowestAssignments(perEmail) {
    let min;
    let taskToBeAssigned;
    Object.keys(perEmail).forEach(taskName => {
        const perTask = perEmail[taskName];
        if (!perTask.created) return;

        const cnt = perTask.assigned;
        if (min === undefined) {
            min = cnt;
            taskToBeAssigned = taskName;
        } else if (cnt < min) {
            min = cnt;
            taskToBeAssigned = taskName;
        }
    });
    return taskToBeAssigned;
}

function getTotalNumberOfAssignments(perEmail) {
    let total = 0;
    Object.keys(perEmail).forEach(taskName => {total += perEmail[taskName].assigned;});
    return total;
}

function calculateAssignments(buckets) {
    const assignments = {};
    Object.keys(buckets).forEach(bucketName => {
        const tasksLimit = getTasksLimit(bucketName);

        // console.log('Tasks limit:', bucketName, tasksLimit);

        const bucket = buckets[bucketName];
        Object.keys(bucket).forEach(email => {
            // console.log('###');
            // console.log('email:', email);

            const perEmail = bucket[email];

            const totalAssignments = getTotalNumberOfAssignments(perEmail);
            // console.log('Total number of assignments:', totalAssignments);

            let more = 0;
            if (tasksLimit > totalAssignments) more = tasksLimit - totalAssignments;

            // console.log('More to assign:', more);

            const toBeAssigned = {};
            while (more) {
                const taskName = findTaskWithLowestAssignments(perEmail);
                if (!taskName) break;
                perEmail[taskName].assigned++;
                perEmail[taskName].created--;

                if (toBeAssigned[taskName] === undefined)
                    toBeAssigned[taskName] = 1;
                else
                    toBeAssigned[taskName]++;

                more--;
            }

            // console.log('toBeAssigned:', email, toBeAssigned);
            assignments[email] = toBeAssigned;
            // console.log('###');
        });
    });
    return assignments;
}

async function getAssignments(dbtx) {
    const buckets = await fillInBuckets(dbtx.db);
    // console.log('buckets:', JSON.stringify(buckets, null, 2));

    const assignments = calculateAssignments(buckets);
    // console.log('assignments:', assignments);
    return assignments;
}