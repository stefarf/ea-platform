const styleH1 = 'style="padding:0.5rem;background-color:rgb(117, 98, 77);color:#ffffff;text-align:center"';
const styleH4 = 'style="font-size:1.5rem;margin-top:1rem;margin-bottom:0.5rem"';
const styleOUL = 'style="margin-top:0;margin-bottom:1rem"';
const styleLI = 'style="font-size:1rem;margin-top:0;margin-bottom:0.5rem"';
const styleA = 'style="font-size:1rem"';
const styleP = 'style="font-size:1rem"';
const styleSubtitle = 'style="font-size:0.85rem"';
const styleTRHead = 'style="background-color:#f3c901"';
const styleTRRow = 'style="background-color:white"';

const notes = `
<h4 ${styleH4}>Notes:</h4>
<ol ${styleOUL}>
    <li ${styleLI}>
        <b>DO NOT</b> forward this email to others because the link contains your access.
    </li>
    <li ${styleLI}>
        <p ${styleP}>If your browser display nothing or the page doesn't showed up properly:</p>
        <ul ${styleOUL}>
            <li ${styleLI}>
                You may need to <b>upgrade your browser </b> because you are using the older one.
            </li>
            <li ${styleLI}>
                You may need to <b>clear your browser's cache</b> to resolve issues related to recent application upgrade.
            </li>
        </ul>
    </li>
</ol>`;

const registerSource = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<h1 ${styleH1}>{{title}}</h1>

{{#if emailFor}}
<p ${styleP}>For: {{emailFor}}</p>
{{/if}}

{{#each messages}}
<p ${styleP}>{{.}}</p>
{{/each}}

<p></p>
<a href="{{{url}}}" ${styleA}>Click here to create your password (valid for {{validFor}})</a>

${notes}
</body>
</html>`;

const notifyForTasks = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<h1 ${styleH1}>{{title}}</h1>

{{#if emailFor}}
<p ${styleP}>For: {{emailFor}}</p>
{{/if}}

{{#each messages}}
<p ${styleP}>{{.}}</p>
{{/each}}

{{#if tasks}}
<div style="margin-top:1rem">
    <table cellspacing="0" cellpadding="5" style="border:1px solid black">
        <tr ${styleTRHead} align="left">
            <th></th>
            <th>Task</th>
        </tr>

        {{#each tasks}}
        <tr ${styleTRRow} valign="top">
            <td><p ${styleP}>{{no}}.</p></td>
            <td>
                <p ${styleP}>{{title}}</p>
                <p ${styleSubtitle}>{{subtitle}}</p>
            </td>
        </tr>
        {{/each}}

        {{#if more}}
        <tr ${styleTRRow} valign="top">
            <td><p ${styleP}></p></td>
            <td><p ${styleP}>... and {{more}} more ...</p></td>
        </tr>
        {{/if}}

    </table>
</div>
{{/if}}

<p></p>
<a href="{{{url}}}" ${styleA}>Click here to open the web app (valid for {{validFor}})</a>

${notes}
</body>
</html>`;

const hbars = require('handlebars');

exports.register = hbars.compile(registerSource);
exports.notify = hbars.compile(notifyForTasks);