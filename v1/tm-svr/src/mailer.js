const httplib = require('http-lib');
const http = require('http'); // const https = require('https');

const config = require('./config').load();

async function send(to, subject, html) {
    const rst = await httplib.request(
        http,
        config.sendEmail.api,
        JSON.stringify({To: to, Subject: subject, HTML: html}));
    console.log('Result:', rst);
}

exports.send = send;