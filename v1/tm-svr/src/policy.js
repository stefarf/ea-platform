function passwordPolicy(password) {
    const comply =
        password.length >= 8 &&
        /[a-z]/.test(password) &&
        /[A-Z]/.test(password) &&
        /[0-9]/.test(password) &&
        /[!@#$%^&*]+/.test(password);
    if (!comply) return 'Password must be at least 8 characters and contains: \n' +
        '- lower case letters,\n' +
        '- upper case letters,\n' +
        '- numbers and\n' +
        '- at least one of these special characters: !@#$%^&*';
}

module.exports = {passwordPolicy};