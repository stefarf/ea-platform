const fix = require('./config').test.fix;

test('Empty config is populated with default', () => {
    expect(fix({})).toEqual({
        baseURL: 'http://localhost:8080',
        server: {port: 2001},

        jwt: {
            key: 'secret',
            loginExpiresIn: '12h',
            registerExpiresIn: '1h',
            apiExpiresIn: '1h',
            buttonExpiresIn: '1h'
        },
        template: {
            registerValidFor: '1 hour'
        },

        mysql: {
            host: 'localhost',
            port: 3306,
            user: 'root',
            password: 'root',
            database: 'mysql',
            cerFile: ''
        },

        sendEmail: {send: false, to: []},

        index: {
            view: 'index'
        },

        menuList: [],
        menus: {},
        tasks: {},
        views: {index: {title: 'INDEX'}},
        dictionaries: {},

        buckets: {default: {tasksLimit: 20}}
    });
});