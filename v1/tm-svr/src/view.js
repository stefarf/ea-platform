const check = require('check');
const sha256 = require('js-sha256').sha256;
const httplib = require('http-lib');
const path = require('path');

const access = require('./access');
const config = require('./config').load();
const global = require('./global');
const jwt = require('./jwt');
const lib = require('./lib');

exports.paramsArgsToData = paramsArgsToData;
exports.generateView = generateView;

function paramsArgsToData(params, args, data) {
    if (params.length !== args.length) throw 'Error unmatch params and args length';
    params = lib.clone(params);
    args = lib.clone(args);
    while (params.length) data[params.shift()] = args.shift();
}

function mergeElementAndDictionary(el, dict) {
    if (dict[el.name])
        Object.keys(dict[el.name]).forEach(key => {
            if (!el[key]) el[key] = dict[el.name][key];
        });
}

function fixConfig(view) {
    view.params = view.params || [];
    view.data = view.data || {};
    view.pages = view.pages || [{}]; // default: one page, no tab
    view.$render = 0;
}

async function generateView(viewName, args, dbtx, auth, opt) {
    const view = config.views[viewName];
    if (!view) throw {code: httplib.statusInternalServerError, err: 'Error undefined view'};

    if (!view.$once) fixConfig(view);

    const data = JSON.parse(JSON.stringify(view.data)); // use cloned data
    const pages = view.pages;

    try {
        paramsArgsToData(view.params, args, data);
    } catch (e) {
        console.error(e);
        throw {code: httplib.statusBadRequest, err: 'Error parameters / arguments does not match'};
    }

    if (!view.$once) {
        if (pages)
            try {
                await lib.asyncForEach(pages, async page => {

                    // Merge elements with dictionary and
                    // add $render for re-render capability
                    if (page.form && page.form.elements) {
                        const dict = page.form.dictionary ? config.dictionaries[page.form.dictionary] : null;
                        page.form.elements.forEach(el => {
                            if (dict) mergeElementAndDictionary(el, dict);
                            el.$render = 0;
                            if (el.elements) {
                                const dict = el.dictionary ? config.dictionaries[el.dictionary] : null;
                                el.elements.forEach(el => {
                                    if (dict) mergeElementAndDictionary(el, dict);
                                    el.$render = 0;
                                });
                                delete el.dictionary;
                            }
                        });
                        delete page.form.dictionary;
                    }

                });
            } catch (e) {
                console.error(e);
                throw {code: httplib.statusInternalServerError, err: 'Internal error'};
            }
        view.$once = true;
    }

    if (pages)
        try {
            let pageIndex = 0;
            await lib.asyncForEach(pages, async page => {

                // Button token
                if (page.form && page.form.buttons) {
                    let buttonIndex = 0;
                    await lib.asyncForEach(page.form.buttons, async b => {
                        if (b.submit) {
                            const jwtData = {view: viewName, pageIndex, buttonIndex, args, email: auth.email};
                            if (opt && opt.taskId !== undefined) jwtData.taskId = opt.taskId;
                            b.$token = await jwt.signHS256(
                                jwtData, config.jwt.key,
                                {subject: 'submit', expiresIn: config.jwt.buttonExpiresIn});
                        }
                        buttonIndex++;
                    });
                }

                // Widget api token
                if (page.widgets) {
                    let widgetIndex = 0;
                    await lib.asyncForEach(page.widgets, async w => {
                        if (w.api)
                            w.api.$token = await jwt.signHS256(
                                {view: viewName, pageIndex, widgetIndex, args, email: auth.email}, config.jwt.key,
                                {subject: 'api', expiresIn: config.jwt.apiExpiresIn});

                        widgetIndex++;
                    });
                }

                pageIndex++;
            });
        } catch (e) {
            console.error(e);
            throw {code: httplib.statusInternalServerError, err: 'Internal error'};
        }

    const ctx = {
        email: auth.email,
        data,
        debug: !!view.debug,
        title: view.title || '',
        pages,
        $bundle: view.$bundle,
        $hooks: view.$hooks,
        $render: view.$render,
        ...opt.ctx
    };

    if (view.finalize) {
        const thisFinalize = {
            ...dbtx,
            auth,
            access: {
                async anyTraits(traits) {return await access.anyTraits(dbtx.db, auth.email, traits);},
                async allTraits(traits) {return await access.allTraits(dbtx.db, auth.email, traits);}
            }
        };

        let err;
        try {
            err = await lib.asyncCall(view.finalize, thisFinalize, ctx);
        } catch (e) {
            console.error(e);
            throw {code: httplib.statusInternalServerError, err: 'Error finalize view'};
        }
        if (err)
            throw {
                code: httplib.statusUnauthorized,
                err: typeof err === 'string' ? err : 'Unauthorized access'
            };
    }

    return ctx;
}