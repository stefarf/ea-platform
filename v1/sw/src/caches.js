export function readCacheIfNoneThenFetchAndStore(req, cacheName) {
    return caches.match(req.url)
        .then(res => {
            if (res) {
                // console.log('Response from cache:', req.url);
                return res;
            }
            return fetch(req)
                .then(res => {
                    let res2 = res.clone();
                    caches.open(cacheName)
                        .then(function (cache) {
                            // console.log('Store response to cache:', req.url);
                            cache.put(req, res2);
                        });
                    // console.log('Response from fetch:', req.url);
                    return res;
                });
        });
}

export function fetchAndStoreIfFailedThenReadCache(req, cacheName) {
    return fetch(req)
        .then(res => {
            let res2 = res.clone();
            caches.open(cacheName)
                .then(cache => {
                    // console.log('Store response to cache:', req.url);
                    cache.put(req, res2);
                });
            // console.log('Response from fetch:', req.url);
            return res;
        })
        .catch(err => {
            // console.log('Catch error:', err);
            return caches.match(req.url)
                .then(res => {
                    if (res) {
                        // console.log('Response from cache:', req.url);
                        return res;
                    }
                    // console.log('Throw error');
                    throw err;
                });
        });
}