export function urlStartsWith(url, starts) {
    return url.slice(0, starts.length) === starts;
}

export function urlContains(url, contains) {
    return !!(url.indexOf(contains) + 1);
}

// var skipURLContain = [
//     '/api/fetch',
//     '/api/menus',
//     '/api/tasks',
// ];
//
// function urlContainSkip(url) {
//     for (var s of skipURLContain) if (url.indexOf(s) + 1) return true;
//     return false;
// }

// var cacheURLHead = [
//     'http://localhost:2000/cache/',
//     'https://ea.btpn.com/cache/',
// ];
//
// function urlIsCacheResources(url) {
//     for (var s of cacheURLHead) if (url.slice(0, s.length) === s) return true;
//     return false;
// }
