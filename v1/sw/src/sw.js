import {urlStartsWith} from './lib';
import {readCacheIfNoneThenFetchAndStore} from './caches';
import {appCacheName, configCacheName, vendorCacheName} from './config';

self.addEventListener('install', ev => {
    console.log('Install SW:', appCacheName);
});

self.addEventListener('activate', ev => {
    console.log('Activate SW:', appCacheName);
    ev.waitUntil(
        caches.keys()
            .then(function (keys) {
                return Promise.all(keys.map(key => {
                    if (key !== appCacheName &&
                        key !== vendorCacheName &&
                        key !== configCacheName) return caches.delete(key);
                }));
            })
    );
});

self.addEventListener('fetch', ev => {
    if (ev.request.method !== 'GET') return; // skip if not GET

    if (urlStartsWith(ev.request.url, 'https://fonts.googleapis.com/') ||
        urlStartsWith(ev.request.url, 'https://fonts.gstatic.com/')) {
        ev.respondWith(readCacheIfNoneThenFetchAndStore(ev.request, vendorCacheName));
        return;
    }

    // if (urlStartsWith(ev.request.url, 'https://ea.btpn.com/css/') ||
    //     urlStartsWith(ev.request.url, 'https://ea.btpn.com/js/') ||
    //     urlContains(ev.request.url, '/app.js') ||
    //     urlContains(ev.request.url, '/favicon.ico') ||
    //     urlContains(ev.request.url, '/manifest.json')) {
    //     ev.respondWith(fetchAndStoreIfFailedThenReadCache(ev.request, appCacheName));
    //     return;
    // }

    // if (urlContains(ev.request.url, '/api/fetch/') ||
    //     urlContains(ev.request.url, '/api/v2/fetch/')) {
    //     ev.respondWith(fetchAndStoreIfFailedThenReadCache(ev.request, configCacheName));
    //     return;
    // }

    console.log('[SW] Fetch:', ev.request.url);
});