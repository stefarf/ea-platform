const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './src/sw.js',
    output: {
        path: path.resolve(__dirname, '../base-ui/public'),
        filename: 'sw.js',
    },

    //
    // Below are typical configuration
    //

    mode: 'production',
    optimization: {
        minimizer: [new UglifyJsPlugin()]
    },
    plugins: [
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    }
};