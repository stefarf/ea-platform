#!/usr/bin/env bash
if [ -z "$EA_PLATFROM_INITIALIZED" ]; then echo EA Platform need to be initialized first; exit; fi

cd $EA_PLATFORM_DIR/DOCKER
docker build -t $EA_PLATFORM_IMAGE .
