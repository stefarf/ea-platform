import Vue from 'vue';
import {fetchAndCompile} from './bundle';

export function registerAsyncComponents(asyncComponents) {
    const nameToURL = {};
    asyncComponents.forEach(def => {
        def.components.forEach(name => {
            nameToURL[name] = def.url;
        });
    });
    Object.keys(nameToURL).forEach(name => {
        const url = nameToURL[name];
        Vue.component(name, () => {
            return fetchAndCompile(url)
                .then(bundle => {
                    return bundle.components[name];
                });
        });
    });
}