import Vue from 'vue';
import {
    VAlert,
    VApp,
    VAutocomplete,
    VAvatar,
    VBadge,
    VBottomNav,
    VBottomSheet,
    VBottomSheetTransition,
    VBreadcrumbs,
    VBreadcrumbsDivider,
    VBreadcrumbsItem,
    VBtn,
    VBtnToggle,
    VCard,
    VCardActions,
    VCardMedia,
    VCardText,
    VCardTitle,
    VCarousel,
    VCarouselItem,
    VCarouselReverseTransition,
    VCarouselTransition,
    VCheckbox,
    VChip,
    VCombobox,
    VContainer,
    VContent,
    VCounter,
    VDataIterator,
    VDataTable,
    VDatePicker,
    VDatePickerDateTable,
    VDatePickerHeader,
    VDatePickerMonthTable,
    VDatePickerTitle,
    VDatePickerYears,
    VDialog,
    VDialogBottomTransition,
    VDialogTransition,
    VDivider,
    VEditDialog,
    VExpandTransition,
    VExpansionPanel,
    VExpansionPanelContent,
    VFabTransition,
    VFadeTransition,
    VFlex,
    VFooter,
    VForm,
    VIcon,
    VImg,
    VInput,
    VItem,
    VItemGroup,
    VLabel,
    VLayout,
    VList,
    VListGroup,
    VListTile,
    VListTileAction,
    VListTileActionText,
    VListTileAvatar,
    VListTileContent,
    VListTileSubTitle,
    VListTileTitle,
    VMenu,
    VMenuTransition,
    VMessages,
    VNavigationDrawer,
    VOverflowBtn,
    VPagination,
    VParallax,
    VPicker,
    VProgressCircular,
    VProgressLinear,
    VRadio,
    VRadioGroup,
    VRangeSlider,
    VRating,
    VResponsive,
    VRowExpandTransition,
    VScaleTransition,
    VScrollXReverseTransition,
    VScrollXTransition,
    VScrollYReverseTransition,
    VScrollYTransition,
    VSelect,
    VSlider,
    VSlideXReverseTransition,
    VSlideXTransition,
    VSlideYReverseTransition,
    VSlideYTransition,
    VSnackbar,
    VSpacer,
    VSpeedDial,
    VStepper,
    VStepperContent,
    VStepperHeader,
    VStepperItems,
    VStepperStep,
    VSubheader,
    VSwitch,
    VSystemBar,
    VTab,
    VTabItem,
    VTableOverflow,
    VTabReverseTransition,
    VTabs,
    VTabsItems,
    VTabsSlider,
    VTabTransition,
    VTextarea,
    VTextField,
    VTimeline,
    VTimelineItem,
    VTimePicker,
    VTimePickerClock,
    VTimePickerTitle,
    VToolbar,
    VToolbarItems,
    VToolbarSideIcon,
    VToolbarTitle,
    VTooltip,
    VTreeview,
    VTreeviewNode,
    VWindow,
    VWindowItem
} from 'vuetify/lib';

// VCalendar,
// VCalendarDaily,
// VCalendarMonthly,
// VCalendarWeekly,
// VExpandXTransition,
// VSheet,
// VSparkline,

export function registerVuetifyComponents() {
    Vue.component('v-app', VApp);
    Vue.component('v-alert', VAlert);
    Vue.component('v-autocomplete', VAutocomplete);
    Vue.component('v-avatar', VAvatar);
    Vue.component('v-badge', VBadge);
    Vue.component('v-bottom-nav', VBottomNav);
    Vue.component('v-bottom-sheet', VBottomSheet);
    Vue.component('v-bottom-sheet-transition', VBottomSheetTransition);
    Vue.component('v-breadcrumbs', VBreadcrumbs);
    Vue.component('v-breadcrumbs-divider', VBreadcrumbsDivider);
    Vue.component('v-breadcrumbs-item', VBreadcrumbsItem);
    Vue.component('v-btn', VBtn);
    Vue.component('v-btn-toggle', VBtnToggle);
    // Vue.component('v-calendar', VCalendar);
    // Vue.component('v-calendar-daily', VCalendarDaily);
    // Vue.component('v-calendar-monthly', VCalendarMonthly);
    // Vue.component('v-calendar-weekly', VCalendarWeekly);
    Vue.component('v-card', VCard);
    Vue.component('v-card-actions', VCardActions);
    Vue.component('v-card-media', VCardMedia);
    Vue.component('v-card-text', VCardText);
    Vue.component('v-card-title', VCardTitle);
    Vue.component('v-carousel', VCarousel);
    Vue.component('v-carousel-item', VCarouselItem);
    Vue.component('v-carousel-reverse-transition', VCarouselReverseTransition);
    Vue.component('v-carousel-transition', VCarouselTransition);
    Vue.component('v-checkbox', VCheckbox);
    Vue.component('v-chip', VChip);
    Vue.component('v-combobox', VCombobox);
    Vue.component('v-container', VContainer);
    Vue.component('v-content', VContent);
    Vue.component('v-coutner', VCounter);
    Vue.component('v-data-iterator', VDataIterator);
    Vue.component('v-data-table', VDataTable);
    Vue.component('v-date-picker', VDatePicker);
    Vue.component('v-date-picker-date-table', VDatePickerDateTable);
    Vue.component('v-date-picker-header', VDatePickerHeader);
    Vue.component('v-date-picker-month-table', VDatePickerMonthTable);
    Vue.component('v-date-picker-title', VDatePickerTitle);
    Vue.component('v-date-picker-years', VDatePickerYears);
    Vue.component('v-dialog', VDialog);
    Vue.component('v-dialog-bottom-transition', VDialogBottomTransition);
    Vue.component('v-dialog-transition', VDialogTransition);
    Vue.component('v-divider', VDivider);
    Vue.component('v-edit-dialog', VEditDialog);
    Vue.component('v-expand-transition', VExpandTransition);
    // Vue.component('v-expand-x-transition', VExpandXTransition);
    Vue.component('v-expansion-panel', VExpansionPanel);
    Vue.component('v-expansion-panel-content', VExpansionPanelContent);
    Vue.component('v-fab-transition', VFabTransition);
    Vue.component('v-fade-transition', VFadeTransition);
    Vue.component('v-flex', VFlex);
    Vue.component('v-footer', VFooter);
    Vue.component('v-form', VForm);
    Vue.component('v-icon', VIcon);
    Vue.component('v-img', VImg);
    Vue.component('v-input', VInput);
    Vue.component('v-item', VItem);
    Vue.component('v-item-group', VItemGroup);
    Vue.component('v-label', VLabel);
    Vue.component('v-layout', VLayout);
    Vue.component('v-list', VList);
    Vue.component('v-list-group', VListGroup);
    Vue.component('v-list-tile', VListTile);
    Vue.component('v-list-tile-action', VListTileAction);
    Vue.component('v-list-tile-action-text', VListTileActionText);
    Vue.component('v-list-tile-avatar', VListTileAvatar);
    Vue.component('v-list-tile-content', VListTileContent);
    Vue.component('v-list-tile-sub-title', VListTileSubTitle);
    Vue.component('v-list-tile-title', VListTileTitle);
    Vue.component('v-menu', VMenu);
    Vue.component('v-menu-transition', VMenuTransition);
    Vue.component('v-messages', VMessages);
    Vue.component('v-navigation-drawer', VNavigationDrawer);
    Vue.component('v-overflow-btn', VOverflowBtn);
    Vue.component('v-pagination', VPagination);
    Vue.component('v-parallax', VParallax);
    Vue.component('v-picker', VPicker);
    Vue.component('v-progress-circular', VProgressCircular);
    Vue.component('v-progress-linear', VProgressLinear);
    Vue.component('v-radio', VRadio);
    Vue.component('v-radio-group', VRadioGroup);
    Vue.component('v-range-slider', VRangeSlider);
    Vue.component('v-rating', VRating);
    Vue.component('v-responsive', VResponsive);
    Vue.component('v-row-expand-transition', VRowExpandTransition);
    Vue.component('v-scale-transition', VScaleTransition);
    Vue.component('v-scroll-x-reverse-transition', VScrollXReverseTransition);
    Vue.component('v-scroll-x-transition', VScrollXTransition);
    Vue.component('v-scroll-y-reverse-transition', VScrollYReverseTransition);
    Vue.component('v-scroll-y-transition', VScrollYTransition);
    Vue.component('v-select', VSelect);
    // Vue.component('v-sheet', VSheet);
    Vue.component('v-slider', VSlider);
    Vue.component('v-slide-x-reverse-transition', VSlideXReverseTransition);
    Vue.component('v-slide-x-transition', VSlideXTransition);
    Vue.component('v-slide-y-reverse-transition', VSlideYReverseTransition);
    Vue.component('v-slide-y-transition', VSlideYTransition);
    Vue.component('v-snackbar', VSnackbar);
    Vue.component('v-spacer', VSpacer);
    // Vue.component('v-sparkline', VSparkline);
    Vue.component('v-speed-dial', VSpeedDial);
    Vue.component('v-stepper', VStepper);
    Vue.component('v-stepper-content', VStepperContent);
    Vue.component('v-stepper-header', VStepperHeader);
    Vue.component('v-stepper-items', VStepperItems);
    Vue.component('v-stepper-step', VStepperStep);
    Vue.component('v-subheader', VSubheader);
    Vue.component('v-switch', VSwitch);
    Vue.component('v-system-bar', VSystemBar);
    Vue.component('v-tab', VTab);
    Vue.component('v-tab-item', VTabItem);
    Vue.component('v-table-overflow', VTableOverflow);
    Vue.component('v-tab-reverse-transition', VTabReverseTransition);
    Vue.component('v-tabs', VTabs);
    Vue.component('v-tabs-items', VTabsItems);
    Vue.component('v-tabs-slider', VTabsSlider);
    Vue.component('v-tab-transition', VTabTransition);
    Vue.component('v-textarea', VTextarea);
    Vue.component('v-text-field', VTextField);
    Vue.component('v-timeline', VTimeline);
    Vue.component('v-timeline-item', VTimelineItem);
    Vue.component('v-time-picker', VTimePicker);
    Vue.component('v-time-picker-clock', VTimePickerClock);
    Vue.component('v-time-picker-title', VTimePickerTitle);
    Vue.component('v-toolbar', VToolbar);
    Vue.component('v-toolbar-items', VToolbarItems);
    Vue.component('v-toolbar-side-icon', VToolbarSideIcon);
    Vue.component('v-toolbar-title', VToolbarTitle);
    Vue.component('v-tooltip', VTooltip);
    Vue.component('v-treeview', VTreeview);
    Vue.component('v-treeview-node', VTreeviewNode);
    Vue.component('v-window', VWindow);
    Vue.component('v-window-item', VWindowItem);
}