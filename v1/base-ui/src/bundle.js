const compiled = {};
const inProgress = {};

export function fetchAndCompile(url) {
    return new Promise(resolve => {
        if (compiled[url]) {
            resolve(compiled[url]);
            return;
        }

        const cb = bundle => {resolve(bundle);};
        if (inProgress[url]) {
            inProgress[url].push(cb);
            return;
        }
        inProgress[url] = [cb];

        fetch(url)
            .then(res => res.text())
            .then(js => {
                const bundle = (function () {
                    eval(js);
                    return this.bundle;
                }.call({}));

                compiled[url] = bundle;
                inProgress[url].forEach(cb => {cb(bundle);});
                delete inProgress[url];
            });
    });
}