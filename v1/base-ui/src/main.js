import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import {vueUseStyle} from './plugins/vuetify';
import {registerVuetifyComponents} from './register-vuetify';
import {registerAsyncComponents} from './register-async';
import {fetchAndCompile} from './bundle';

Vue.config.productionTip = false;
fetch('/load/config.json')
    .then(res => res.json())
    .then(config => {

        // Style
        if (config.style) vueUseStyle(config.style);

        // Title
        document.title = config.title || 'V-App Platform';

        // Register service worker
        if (navigator.serviceWorker && config.sw) {
            navigator.serviceWorker.register(config.sw.url).then(() => {
            });
        }

        // Register components
        registerVuetifyComponents();

        let asyncComponents = [];
        if (config.asyncComponents) asyncComponents = config.asyncComponents;

        const registerAsyncComponentsAndProceedToRoutes = () => {
            registerAsyncComponents(asyncComponents);

            if (config.routes) {

                // Use router
                Vue.use(VueRouter);
                fetchAndCompile(config.routes.url)
                    .then(bundle => {
                        new Vue({
                            router: new VueRouter({routes: bundle.routes, mode: 'history'}),
                            render: h => h(App)
                        }).$mount('#app');
                    });

            } else {

                // Run Vue right away
                new Vue({
                    render: h => h(App)
                }).$mount('#app');

            }
        };
        if (config.custom)
            fetch(config.custom.url)
                .then(res => res.json())
                .then(custom => {
                    if (custom.asyncComponents)
                        asyncComponents = asyncComponents.concat(custom.asyncComponents);
                    registerAsyncComponentsAndProceedToRoutes();
                });
        else
            registerAsyncComponentsAndProceedToRoutes();

    });