import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

export function vueUseStyle(style) {Vue.use(Vuetify, style);}