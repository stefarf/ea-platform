module.exports = {
    runtimeCompiler: true,
    pluginOptions: {
        webpackBundleAnalyzer: {
            openAnalyzer: false
        }
    }
};