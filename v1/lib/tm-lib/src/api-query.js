exports.composeWhere = composeWhere;

function composeWhere(arr, op) {
    let where = '';
    arr.forEach(w => {
        if (where) where += ` ${op} `;
        where += `(${w})`;
    });
    return where ? 'WHERE ' + where : where;
}
