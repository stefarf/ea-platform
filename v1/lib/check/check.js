//
// https://webbjocke.com/javascript-check-data-types/
//

exports.isString = function (value) {
    return typeof value === 'string' || value instanceof String;
};

exports.isNumber = function (value) {
    return typeof value === 'number' && isFinite(value);
};

// ES5 actually has a method for this (ie9+)
// Array.isArray(value);
exports.isArray = function (value) {
    return value && typeof value === 'object' && value.constructor === Array;
};

exports.isFunction = function (value) {
    return typeof value === 'function';
};

exports.isObject = function (value) {
    return value && typeof value === 'object' && value.constructor === Object;
};

exports.isNull = function (value) {
    return value === null;
};

exports.isUndefined = function (value) {
    return typeof value === 'undefined';
};

exports.isBoolean = function (value) {
    return typeof value === 'boolean';
};

exports.isRegExp = function (value) {
    return value && typeof value === 'object' && value.constructor === RegExp;
};

exports.isError = function (value) {
    return value instanceof Error && typeof value.message !== 'undefined';
};

exports.isDate = function (value) {
    return value instanceof Date;
};

exports.isSymbol = function (value) {
    return typeof value === 'symbol';
};