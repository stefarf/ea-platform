const url = require('url');
const http = require('http');
const https = require('https');
const HttpsProxyAgent = require('https-proxy-agent');

//
// EXPORTS
//

exports.request = request;
exports.request2 = request2;

// Frequently used:
exports.statusOK = 200;
exports.statusBadRequest = 400;
exports.statusUnauthorized = 401;
exports.statusInternalServerError = 500;

// Rarely used:
exports.statusCreated = 201;
exports.statusNoContent = 204;
exports.statusNotModified = 304;
exports.statusForbidden = 403;
exports.statusNotFound = 404;
exports.statusConflict = 409;

// The request() is wrapper of http.request / https.request
//
// Arguments:
// - mod: is either require('http') or require('https')
// - opt: is mod.request options
// - body: if defined then it will be written as request body
//
// Returns: {statusCode, body}
// - statusCode: is valid http status code (ex: 200 = OK)
// - body: response body, string
function request(mod, opt, body) {
    return new Promise((resolve, reject) => {
        const req = mod.request(opt, res => {
            let body = '';
            res.on('data', chunk => {
                body += chunk;
            });
            res.on('end', () => {
                resolve({statusCode: res.statusCode, body});
            });
        });
        req.on('error', err => {
            reject(err);
        });
        if (body) req.write(body);
        req.end();
    });
}

// The request2() is a wrapper for request()
//
// Arguments:
// - cfg: {
//     url: 'https://host:port/path/to/resources',
//
//     httpsProxyAgent: true, // use https proxy agent if protocol is https and proxy is defined
//     proxy: 'http://proxyhost:port'
//
//     // request options
//     opt: {/* ... */}
// }
//
// Returns: same as request()
function request2(cfg, body) {
    const opt = {...url.parse(cfg.url), ...cfg.opt};
    const ishttps = opt.protocol === 'https:';
    if (cfg.httpsProxyAgent && cfg.proxy && ishttps) {
        opt.agent = new HttpsProxyAgent(cfg.proxy);
    }
    return request(ishttps ? https : http, opt, body);
}