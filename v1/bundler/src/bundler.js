const fs = require('fs');
const sha256 = require('js-sha256').sha256;
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

const tblHookArgs = {
    beforeSubmit: ['data'],
    buttonClicked: ['buttonName'],
    changed: ['elementName']
};

let bundleIdx = 1;
const bundleProcessed = {};

function compile(opt) {
    return new Promise((resolve, reject) => {
        webpack(opt, (err, stats) => {
            if (err) {
                reject(err);
                return;
            }
            if (stats.hasErrors()) {
                reject(stats);
                return;
            }

            const filename = path.resolve(opt.output.path, opt.output.filename);
            const hash = sha256(fs.readFileSync(filename, 'utf8'));
            console.log('### BUNDLE COMPILED ###', filename, hash);
            resolve({filename, hash});
        });
    });
}

module.exports = function (bundleName, property) {
    let bp = bundleProcessed[bundleName];
    if (!bp) {
        bp = {idx: bundleIdx};
        bundleProcessed[bundleName] = bp;
        bundleIdx++;
    }

    return {
        async bundler($bundle, hookName, global) {
            if (!bp.hash) {
                let filename, hash;
                try {
                    const compiled = await compile({
                        entry: path.resolve(process.env.TM_BUNDLE, 'src', bundleName + '.js'),
                        output: {
                            path: path.resolve(process.env.TM_BUNDLE, 'bundles'),
                            filename: bundleName.replace(/\//g, '-') + '.js',
                            library: 'bundle',
                            libraryTarget: 'this'
                        },
                        mode: 'production',
                        optimization: {
                            minimize: true,
                            minimizer: [new UglifyJsPlugin()]
                        },
                        module: {
                            rules: [
                                {
                                    test: /\.js$/,
                                    use: {
                                        loader: 'babel-loader',
                                        options: {presets: ['@babel/preset-env']}
                                    }
                                },
                                {
                                    test: /\.css$/,
                                    use: ['css-loader']
                                }
                            ]
                        }
                    });
                    filename = compiled.filename;
                    hash = compiled.hash;
                } catch (e) {
                    console.error(e);
                    return `function(){alert('Bundler error!')}`;
                }
                bp.hash = hash;
                global.fileToHash[filename] = hash;
                global.hashToFile[hash] = filename;
            }

            $bundle[bp.idx] = bp.hash;

            // Compose args
            let args = '';
            const argsInfo = tblHookArgs[hookName];
            if (argsInfo) args = argsInfo.join(',');
            if (args) args += ',';
            args += 'done,error';

            return `function(${args}){var ctx=this;fc(this.$bundle[${bp.idx}]).then(b=>{b.${property}.call(ctx,${args});}).catch(err=>{error(err);});}`;
        }
    };
};