const path = require('path');

const version = 'v1.5.2';

const TASK_JS = path.resolve(__dirname, `../../base-ui/public/load/task.${version}.js`);
const CONFIG_JSON = path.resolve(__dirname, '../../tm-components/config.json');

const fs = require('fs');
const JSDOM = require('jsdom').JSDOM;
const dom = new JSDOM(`<!DOCTYPE html></html>`);

const js = fs.readFileSync(TASK_JS, 'utf8');
const bundle = (function (document) {
    eval(js);
    return this.bundle;
}.call({}, dom.window.document));

const config = JSON.parse(fs.readFileSync(CONFIG_JSON, 'utf8'));
config.asyncComponents = [{url: `/load/task.${version}.js`, components: Object.keys(bundle.components)}];
fs.writeFileSync(CONFIG_JSON, JSON.stringify(config, null, 2), 'utf8');