package main

import (
	"bitbucket.org/stefarf/httptool/httpmdw"
	"bitbucket.org/stefarf/httptool/httpsvr"
	"bitbucket.org/stefarf/httptool/restcli"
	"bitbucket.org/stefarf/httptool/vuebox"
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"github.com/justinas/alice"
	"github.com/rs/cors"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var (
	corsAllowsAll = cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
	})
)

func main() {
	mux := http.NewServeMux()

	// Use custom.APIMap as the base, add config.APIMap (replace if duplicate from)
	if custom.APIMap == nil {
		custom.APIMap = map[string]string{}
	}
	for from, to := range config.APIMap {
		custom.APIMap[from] = to
	}
	for from, to := range custom.APIMap {
		pattern := "/api" + from
		mux.Handle(pattern,
			http.StripPrefix(pattern,
				corsAllowsAll.Handler(
					apiForwarder(to))))
	}

	vuebox.Box("web", mux)

	var cerFile, keyFile string
	if os.Getenv("EA_CONFIG_MODE") == "prod" &&
		custom.Server.CerFile != "" && custom.Server.KeyFile != "" {
		cerFile = config.Custom.BaseDir + custom.Server.CerFile
		keyFile = config.Custom.BaseDir + custom.Server.KeyFile

		fmt.Printf("[WEBGW] Certificate file: %s\n", cerFile)
		fmt.Printf("[WEBGW] Key file: %s\n", keyFile)
	}

	iferr.Exit(
		httpsvr.ListenAndServe(
			":"+config.Server.Port,
			cerFile,
			keyFile,
			time.Second*time.Duration(config.Limit.Timeout),
			alice.New(
				httpmdw.OpenLimit(config.Limit.OpenLimit),
				httpmdw.TPSLimit(config.Limit.TPSLimit, config.Limit.TPSBurst),
				httpmdw.RequestSizeLimit(config.Limit.RequestSizeLimit),
			).Then(mux)), "")
}

func apiForwarder(to string) http.HandlerFunc {
	return http.HandlerFunc(
		func(res http.ResponseWriter, req *http.Request) {

			// Define url
			url := to + req.URL.Path
			fmt.Printf("[api-forwarder] %s\n", url)

			// Read body
			body, err := ioutil.ReadAll(req.Body)
			if err != nil {
				fmt.Printf("[api-forwarder] %s => error read body\n", url)
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

			// Call client api
			cliRes, err := restcli.Method(
				req.Method, url, time.Second*time.Duration(config.Limit.Timeout), req.Header, body)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

			// Response
			for key, vals := range cliRes.Response.Header {
				for _, val := range vals {
					res.Header().Add(key, val)
				}
			}
			res.WriteHeader(cliRes.StatusCode)
			res.Write(cliRes.Bytes)

		})
}
