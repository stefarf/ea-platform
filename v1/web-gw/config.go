package main

import (
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var config struct {
	APIMap map[string]string `yaml:"api_map"`

	Custom struct {
		Config  string
		BaseDir string `yaml:"base_dir"`
	}

	Server struct {
		Port string
	}

	Limit struct {
		Timeout          int
		OpenLimit        int     `yaml:"open_limit"`
		TPSLimit         float64 `yaml:"tps_limit"`
		TPSBurst         float64 `yaml:"tps_burst"`
		RequestSizeLimit int64   `yaml:"request_size_limit"`
	}
}

var custom struct {
	APIMap map[string]string `yaml:"api_map"`
	Server struct {
		CerFile string `yaml:"cer_file"`
		KeyFile string `yaml:"key_file"`
	}
}

func init() {
	readConfig("### Read web-gw-yaml ###", "web-gw.yaml", &config)
	if config.Custom.Config != "" {
		readConfig("### Read custom API map config ###", config.Custom.Config, &custom)
	}
}

func readConfig(msg, full string, v interface{}) {
	fmt.Println(msg)
	errMsg := fmt.Sprintf("error reading/parsing configuration file '%s'", full)
	b, err := ioutil.ReadFile(full)
	iferr.Exit(err, errMsg)
	iferr.Exit(yaml.Unmarshal(b, v), errMsg)
}
